const priceInput = document.getElementById('price');
const container = document.querySelector('.container');

priceInput.addEventListener('focus', function() {
  priceInput.classList.remove('price-input-error');
});

priceInput.addEventListener('blur', function() {
  const priceValue = parseFloat(priceInput.value);
  if (priceValue < 0 || isNaN(priceValue)) {
    priceInput.classList.add('price-input-error');
    container.querySelector('.price-info').remove();
    const errorMessage = document.createElement('span');
    errorMessage.textContent = 'Please enter correct price.';
    errorMessage.classList.add('price-info');
    container.appendChild(errorMessage);
  } else {
    priceInput.style.borderColor = '#ccc';
    const priceInfo = document.createElement('span');
    priceInfo.textContent = `Current price: ${priceValue}`;
    priceInfo.classList.add('price-info');
    const closeButton = document.createElement('span');
    closeButton.textContent = 'X';
    closeButton.classList.add('close-btn');
    closeButton.addEventListener('click', function() {
      priceInfo.remove();
      priceInput.value = '';
    });
    container.appendChild(priceInfo);
    priceInfo.appendChild(closeButton);
  }
});
